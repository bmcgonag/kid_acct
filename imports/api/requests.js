import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Requests = new Mongo.Collection('requests');

Requests.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.request' (type, reqFrom, reqAmt) {
        check(type, String);
        check(reqFrom, String);
        check(reqAmt, Number);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to make requests. Make sure you are logged in with valid user credentials.');
        }

        return Requests.insert({
            reqType: type,
            sendTo: sendTo,
            reqFrom: Meteor.users.emails.address[0],
            reqAmt: reqAmt,
            reqOn: new Date(),
            reqStatus: "Pending",
        });
    },
    'edit.request' (reqId, status, statusReason) {
        check(reqId, String);
        check(status, String);
        check(statusReason, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit requests. Make sure you are logged in with valid user credentials.');
        }

        return Requests.update({ _id: reqId }, {
            $set: {
                reqStatus: status,
                reqStatusReason: statusReason,
                reqStatusUpdatedBy: Meteor.users.emails.address[0],
            }
        });
    }
});