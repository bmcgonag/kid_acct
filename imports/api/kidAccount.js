import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const KidAccount = new Mongo.Collection('kidAccount');

KidAccount.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.kidAccount' (name, username, accountNo, initialBalance, canGoNegative, canBorrow, canRequestPayment, canPayorTrnasfer, canRequestToBuy) {
        check(name, String);
        check(username, String);
        check(accountNo, String);
        check(initialBalance, Number);
        check(canGoNegative, Boolean);
        check(canBorrow, Boolean);
        check(canRequestPayment, Boolean);
        check(canPayorTrnasfer, Boolean);
        check(canRequestToBuy, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add accounts. Make sure you are logged in with valid user credentials.');
        }

        return KidAccount.insert({
            kidName: name,
            kidUsername: username,
            kidAccountNo: accountNo,
            kidBalance: initialBalance,
            canGoNegative: canGoNegative,
            canBorrow: canBorrow,
            canRequestPayment: canRequestPayment,
            canPayorTrnasfer: canPayorTrnasfer,
            canRequestToBuy: canRequestToBuy,
        });
    },
    'edit.kidAccountBalance' (kidId, changeAmount) {
        check(kidId, String);
        check(changeAmount, Number);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit account balances. Make sure you are logged in with valid user credentials.');
        }

        let accountBalanceOrig = KidAccount.findOne({ kidUserId: kidId }).kidBalance;
        let newBalance = accountBalanceOrig + changeAmount;
        console.log("    ---------------------------    ");
        console.log("    New Balance: " + newBalance);
        console.log("    ---------------------------    ");
        return KidAccount.update({ kidUserId: kidId }, {
            $set: {
                kidBalance: newBalance,
            }
        });
    },
    'edit.kidAccountPermissions' (canGoNegative, canBorrow, canRequestPayment, canPayorTrnasfer, canRequestToBuy) {

    },
    'edit.kidName' (kidId, name) {

    },
    'delete.kidAccount' (kidId) {
        check(kidId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete accounts. Make sure you are logged in with valid user credentials.');
        }

        return KidAccount.remove({ _id: kidId });
    },
    'add.kidUserId' (kidusername, kidUserId) {
        check(kidusername, String);
        check(kidUserId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete accounts. Make sure you are logged in with valid user credentials.');
        }

        return KidAccount.update({ kidUsername: kidusername }, {
            $set: {
                kidUserId: kidUserId,
            }
        });
    }
});