import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Transactions = new Mongo.Collection('transactions');

Transactions.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.transaction' (kidId, transactionAmount, transactionNote, requestId) {
        check(kidId, String);
        check(transactionAmount, Number);
        check(transactionNote, String);
        check(requestId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add transactions. Make sure you are logged in with valid user credentials.');
        }

        Transactions.insert({
            kidId: kidId,
            transactionAmount: transactionAmount,
            transactionNote: transactionNote,
            requestId: requestId,
            transactionOn: new Date(),
        });

        Meteor.call('edit.kidAccountBalance', kidId, transactionAmount, function(err, result) {
            if (err) {
                console.log("    ERROR changing kid account balance: " + err);
            } else {
                console.log("    SUCCESS changing kid account balance.");
            }
        });
    },
});