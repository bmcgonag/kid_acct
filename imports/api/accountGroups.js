import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const AccountsGroup = new Mongo.Collection('accountsGroup');

AccountsGroup.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.accountGroup' (groupName) {
        check(groupName, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add account groups. Make sure you are logged in with valid user credentials.');
        }

        return AccountsGroup.insert({
            groupName: groupName,
            addedBy: this.userId,
        });
    },
    'delete.accountsGroup' (groupNameId) {
        check(groupNameId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete account groups. Make sure you are logged in with valid user credentials.');
        }

        return AccountsGroup.remove({ _id: groupNameId });
    },
});