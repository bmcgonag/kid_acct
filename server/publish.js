import { SysConfig } from '../imports/api/systemConfig.js';
import { UserConfigOptions } from '../imports/api/userConfigOptions.js';
import moment from 'moment';
import { AccountsGroup } from '../imports/api/accountGroups.js';
import { KidAccount } from '../imports/api/kidAccount.js';
import { Transactions } from '../imports/api/transactions.js';

Meteor.publish("SystemConfig", function() {
    try {
        return SysConfig.find({});
    } catch (error) {
        console.log("    ERROR pulling system config data: " + error);
    }
});

Meteor.publish("userConfig", function() {
    try {
        return UserConfigOptions.find({});
    } catch (error) {
        console.log("    ERROR pulling user configuration options." + error);
    }
})

Meteor.publish('userList', function() { 
    return Meteor.users.find({});
});

Meteor.publish('accountGroups', function() {
    try {
        return AccountsGroup.find({});
    } catch (error) {
        console.log("    ERROR getting account group information: " + error);
    }
    
});

Meteor.publish('kidAccountInfo', function() {
    try {
        if (Roles.userIsInRole(this.userId, ['systemadmin','parent'])) {
            return KidAccount.find({});
        } else {
            return KidAccount.find({ kidUserId: this.userId });
        }
    } catch (error) {
        console.log("    ERROR getting kid account information: " + error);
    }
    
});

Meteor.publish('transInfo', function() {
    try {
        if (Roles.userIsInRole(this.userId, ['kid'])) {
            return Transactions.find({ kidId: this.userId }, {sort: { transactionOn: -1}, limit: 7});
        } else {
            return Transactions.find({}, {sort: { transactionOn: -1}, limit: 7});
        }
    } catch (error) {
        console.log("    ERROR getting trans information: " + error);
    }
});

