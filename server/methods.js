import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Meteor.methods({
    'addToRole' (role) {
        let countOfUsers = Meteor.users.find().count();

        // if users = 1 - set to role passed in function call
        if (countOfUsers > 1) {
            console.log("User id for role: " + Meteor.userId() );
            let userId = Meteor.userId();
            Roles.addUsersToRoles(userId, role);
        } else if (countOfUsers == 1) {
            console.log("Creating first system admin user: " + Meteor.userId() );
            let userId = Meteor.userId();
            Roles.addUsersToRoles(userId, "systemadmin");
        }
    },
    'change.userPass' (usersId, password) {
        check(usersId, String);
        check(password, String);

        return Accounts.setPassword(usersId, password);
    }
});