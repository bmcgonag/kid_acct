import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  Roles.createRole("kid", {unlessExists: true});
  Roles.createRole("parent", {unlessExists: true});
  Roles.createRole("systemadmin", {unlessExists: true});
});
