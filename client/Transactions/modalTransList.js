import { Transactions } from "../../imports/api/transactions";
import { KidAccount } from "../../imports/api/kidAccount";
import moment from 'moment';

Template.modalTransList.onCreated(function() {
    this.subscribe("transInfo", Session.get("transId"));
    this.subscribe("kidAccount");
});

Template.modalTransList.onRendered(function() {

});

Template.modalTransList.helpers({
    trans: function() {
        return Transactions.find({ kidId: Session.get("transId") });
    },
    transDate: function() {
        return moment(this.transactionOn).format("MM-DD-YYYY");
    },
    transAmt: function() {
        return (this.transactionAmount).toFixed(2);
    },
});

Template.modalTransList.events({
    'click .close' (event) {
        let listModal = document.getElementById("modalTransList");
        listModal.style.display = "none";
    },
    'click .modal-close' (event) {
        let listModal = document.getElementById("modalTransList");
        listModal.style.display = "none";
    },
});