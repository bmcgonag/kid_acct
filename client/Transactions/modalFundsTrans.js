import { KidAccount } from "../../imports/api/kidAccount";
import { Transactions } from '../../imports/api/transactions.js';

Template.modalFundsTrans.onCreated(function() {
    this.subscribe("kidAccountInfo");
    this.subscribe("transInfo");
});

Template.modalFundsTrans.onRendered(function() {
    Session.set("transAmtErr", true);
    Session.set("noNeg", false);
    Session.set("kidUserId", );
});

Template.modalFundsTrans.helpers({
    kidTrans: function() {
        return Session.get("userTransName")
    },
    kidBal: function() {
        let kidInfo = KidAccount.findOne({ kidUserId: Session.get("userTrans")});
        if (typeof kidInfo != 'undefined') {
            return (kidInfo.kidBalance).toFixed(2);
        }
    },
    transAmtErr: function() {
        return Session.get("transAmtErr");
    },
    noNeg: function() {
        return Session.get("noNeg");
    },
});

Template.modalFundsTrans.events({
    'click .makeTrans' (event) {
        event.preventDefault();
        let kidUserId = Session.get("userTrans");
        let kidName = Session.get("userTransName");
        let kidBal = Session.get("userCurrBal");
        let canGoNeg = Session.get("userCanGoNeg");
        let transType = Session.get("transType");
        let transAmt = $("#amount").val();
        let transNote = $("#transNote").val();
        let requestId = "none";

        let transAmtNo = parseFloat(transAmt);

        if (transType == "subtract") {
            transAmtNo = transAmtNo * (-1);
        }

        let newBal = kidBal + transAmtNo;

        if (newBal < 0 && canGoNeg == false) {
            // show a message that says the user can't go negative.
            Session.set("noNeg", true);
            return;
        }

        if (transAmt == null || transAmt == "") {
            Session.set("transAmtErr", true);
        } else {
            console.log("transAmtNo: " + transAmtNo);
            Meteor.call("add.transaction", kidUserId, transAmtNo, transNote, requestId, function(err, result) {
                if (err) {
                    console.log("    ERROR adding transaction: " + err);
                } else {
                    console.log("    SUCCESS adding transaction.");
                    $("#amount").val("");
                    $("#transNote").val("")
                    let editModal = document.getElementById('modalFundsTrans');
                    editModal.style.display = "none";
                }
            });
        }
    },
    'click .cancelTrans' (event) {
        event.preventDefault();
        Session.set("noNeg", false);
        $("#amount").val("");
        $("#transNote").val("");
        let editModal = document.getElementById('modalFundsTrans');
        editModal.style.display = "none";
    },
    'click .close' (event) {
        let editModal = document.getElementById('modalFundsTrans');
        editModal.style.display = "none";
    }
});