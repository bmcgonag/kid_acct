import { KidAccount } from "../../imports/api/kidAccount";
import { Transactions } from "../../imports/api/transactions";
import { Requests } from "../../imports/api/requests";

Template.requestsModal.onCreated(function() {

});

Template.requestsModal.onRendered(function() {

});

Template.requestsModal.helpers({
    reqType: function() {
        return Session.get("reqType");
    },
    users: function() {
        return Meteor.users.find({});
    },
});

Template.requestsModal.events({
    'click .modal-close' (event) {
        Session.set("reqType", "");
        let reqMod = document.getElementById("reqModal");
        reqMod.style.display = "none";
    },
    'click .close' (event) {
        Session.set("reqType", "");
        let reqMod = document.getElementById("reqModal");
        reqMod.style.display = "none";
    },
    'click .submitRequest' (event) {
        // get the type of request being made
        let reqFor;
        let SentTo;
        let reqType = Session.get("reqType");
        let reqAmt = $("#reqAmt").val();
        
        if (reqType == "Semd Money") {
            reqFor = $("#reqFor").val();
        } else {
            sendTo = $("#sentTo").val();
        }

        Meteor.call("add.request", reqType, sendTo, reqAmt, function(err, result) {
            if (err) {
                console.log("Request failed with error: " + err);
            } else {
                console.log("Request succeeded.");
            }
        });

        // close the modal
        let reqMod = document.getElementById("reqModal");
        reqMod.style.display = "none";
    }
});