import { AccountsGroup } from '../../../../imports/api/accountGroups.js';

Template.acctGroupList.onCreated(function() {
    this.subscribe("accountGroups");
});

Template.acctGroupList.onRendered(function() {

});

Template.acctGroupList.helpers({
    acctGroups: function() {
        return AccountsGroup.find({});
    },
});

Template.acctGroupList.events({
    'click .editGroupName' (event) {

    }, 
    'click .deleteGroupName' (event) {
        let groupNameId = this._id;
        let groupName = this.groupName;
        let method = "delete.accountsGroup";
        let view = "Account Group Names";

        Session.set("item", groupName);
        Session.set("view", view);
        Session.set("method", method);
        Session.set("deleteId", groupNameId);

        $('#modalDelete').modal('open');
    },
});