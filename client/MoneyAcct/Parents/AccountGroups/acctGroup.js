import { AccountsGroup } from "../../../../imports/api/accountGroups";

Template.acctGroup.onCreated(function() {
    this.subscribe('accountGroups');
});

Template.acctGroup.onRendered(function() {
    
});

Template.acctGroup.helpers({

});

Template.acctGroup.events({
    'click .addAcctGroup' (event) {
        event.preventDefault();
        let groupName = $("#accountGroupName").val();

        if (groupName == null || groupName == "") {
            console.log("no group name entered: REQUIRED FIELD");
        } else {
            Meteor.call("add.accountGroup", groupName, function(err, result) {
                if (err) {
                    console.log("    ERROR adding account group name: " + err);
                } else {
                    console.log("    SUCCESS adding account group name.");
                }
            });
        }
    },
});