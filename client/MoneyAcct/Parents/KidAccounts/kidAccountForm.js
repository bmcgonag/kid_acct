import { AccountsGroup } from "../../../../imports/api/accountGroups";
import { KidAccount } from '../../../../imports/api/kidAccount.js';

Template.kidAccountForm.onCreated(function() {
    this.subscribe("accountGroup");

});

Template.kidAccountForm.onRendered(function() {
    Session.set("acctNoGen", "");
    Session.set("kidNameErr", false);
    Session.set("kidUserErr", false);
    Session.set("kidPassErr", false);
    Session.set("kidAcctErr", false);
    Session.set("kidInitErr", false);
});

Template.kidAccountForm.helpers({
    nameErr: function() {
        let nameErr = Session.get("kidNameErr");
        console.log(nameErr);
        return nameErr
    },
    userErr: function() {
        return Session.get("kidUserErr");
    },
    passErr: function() {
        return Session.get("kidPassErr");
    },
    acctErr: function() {
        return Session.get("kidAcctErr");
    },
    initErr: function() {
        return Session.get("kidInitErr");
    }
});

Template.kidAccountForm.events({
    'click .autoAcctNo' (event) {
        event.preventDefault();
        let acctNoGen = "";
        let newGen;

        for (i=0; i < 6; i++) {
            newGen = Math.floor(Math.random() * 10);
            acctNoGen = acctNoGen.concat(newGen);
        }
        $("#kidAcctNo").val(acctNoGen);
        // Session.set("acctNoGen", acctNoGen);
    },
    'click .addKidAcct' (event) {
        event.preventDefault();
        let name = $("#kidName").val();
        let username = $("#kidUsername").val();
        let pass = $("#kidPass").val();
        let acctNo = $("#kidAcctNo").val();
        let initBal = $("#kidInitBal").val();

        let canGoNeg = $("#canGoNeg").prop('checked');
        let canBorrow = $("#canBorrow").prop('checked');
        let canReqPay = $("#canReqPay").prop('checked');
        let canTransfer = $("#canTransfer").prop('checked');
        let canReqBuy = $("#canReqBuy").prop('checked');

        let nameErr;
        let userErr;
        let passErr;
        let acctErr;
        let initErr;

        // check to see all fields filled in
        if (name == "" || name == null) {
            Session.set("kidNameErr", true);
            nameErr = true;
        }

        if (username == "" || username == null) {
            Session.set("kidUserErr", true);
            userErr = true;
        }

        if (pass == "" || pass == null) {
            Session.set("kidPassErr", true);
            passErr = true;
        }

        if (acctNo == "" || acctNo == null) {
            Session.set("kidAcctErr", true);
            acctErr = true;
        }

        if (initBal == "" || initBal == null) {
            Session.set("kidInitErr", true);
            initErr = true;
        }

        if (nameErr == true || userErr == true || passErr == true || acctErr == true || initErr == true) {
            return;
        } else {
            let initBalNum = parseFloat(initBal);
            let acctNoStr = acctNo.toString();
            console.log(typeof acctNoStr);
            Meteor.call("add.kidAccount", name, username, acctNoStr, initBalNum, canGoNeg, canBorrow, canReqPay, canTransfer, canReqBuy, function(err, result){
                if (err) {
                    console.log("    ERROR adding kid account: " + err);
                } else {
                    console.log("    SUCCESS adding new kid account.");
                    Accounts.createUser({
                        email: username,
                        password: pass,
                        profile: {
                            fullname: name,
                        }
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            var newUserId = Meteor.userId();
                            
                            console.log("User ID created is: " + newUserId);
;
                            Meteor.call("addToRole", "kid", function(err, result) {
                                if (err) {
                                    console.log("    ERROR: ROLES - Error adding user to role: " + err);
                                } else {
                                    Meteor.call("add.kidUserId", username, newUserId, function(err, result) {
                                        if (err) {
                                            console.log("    ERROR adding userId to Kid account: " + err);
                                        } else {
                                            console.log("    SUCCESS adding userId to Kid account.");
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            });
        }
    }
});