import { KidAccount } from '../../../../imports/api/kidAccount.js';

Template.kidAccountList.onCreated(function() {
    this.subscribe("kidAccountInfo");
});

Template.kidAccountList.onRendered(function() {

});

Template.kidAccountList.helpers({
    kids: function() {
        return KidAccount.find({});
    },
});

Template.kidAccountList.events({

});