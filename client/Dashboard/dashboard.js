import moment from 'moment';
import { AccountsGroup } from '../../imports/api/accountGroups.js';
import { KidAccount } from '../../imports/api/kidAccount.js';

Template.dashboard.onCreated(function() {
    this.subscribe("accountGroups");
    this.subscribe("kidAccountInfo");
});

Template.dashboard.onRendered(function() {

});

Template.dashboard.helpers({
    accountGroupCount: function() {
        return AccountsGroup.find().count();
    },
});

Template.dashboard.events({
    
});