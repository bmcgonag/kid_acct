import { KidAccount } from "../../../imports/api/kidAccount";
import { Transactions } from "../../../imports/api/transactions";
import moment from 'moment';

Template.myKidsCard.onCreated(function() {
    this.subscribe("kidAccountInfo");
    this.autorun( () => {
        this.subscribe("transInfo");
    });
    
});

Template.myKidsCard.onRendered(function() {

});

Template.myKidsCard.helpers({
    kidInfo: function() {
        return KidAccount.find({});
    },
    balAmt: function() {
        return (this.kidBalance).toFixed(2);
    }
});

Template.myKidsCard.events({
    'click .addFunds' (event) {
        event.preventDefault();
        Session.set("userTrans", this.kidUserId);
        Session.set("userTransName", this.kidName);
        Session.set("userCurrBal", this.kidBalance);
        Session.set("userCanGoNeg", this.canGoNegative);
        Session.set("transType", "add");
        // $('#modalFundsTrans').modal('open');
        let editModal = document.getElementById('modalFundsTrans');
        editModal.style.display = "block";
    },
    'click .takeFunds' (event) {
        event.preventDefault();
        Session.set("userTrans", this.kidUserId);
        Session.set("userTransName", this.kidName);
        Session.set("userCurrBal", this.kidBalance);
        Session.set("transType", "subtract");
        Session.set("userCanGoNeg", this.canGoNegative);
        // $('#modalFundsTrans').modal('open');
        let editModal = document.getElementById('modalFundsTrans');
        editModal.style.display = "block";
    },
    'click .transList' (event) {
        event.preventDefault();
        Session.set("transId", this.kidUserId);
        Meteor.setTimeout(function() {
            // $("#modalTransList").modal('open');
            let listModal = document.getElementById('modalTransList');
            listModal.style.display = "block";
        }, 150);
    }
});