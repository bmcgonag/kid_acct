import { KidAccount } from '../../../imports/api/kidAccount.js';
import { Transactions } from '../../../imports/api/transactions.js';

Template.myAcctCard.onCreated(function() {
    this.subscribe("kidAccountInfo");
    this.autorun( () => {
        this.subscribe("transInfo");
    });
});

Template.myAcctCard.onRendered(function() {

});

Template.myAcctCard.helpers({
    kidInfo: function() {
        return KidAccount.findOne({});
    },
    balAmt: function() {
        let kidInfo = KidAccount.findOne({});
        if (kidInfo) {
            let mybal = kidInfo.kidBalance;
            let bal = mybal.toFixed(2);
            return bal;
        }
    },
});

Template.myAcctCard.events({
    'click .transHistory' (event) {
        event.preventDefault();
        let userId = Meteor.userId();;
        Session.set("transId", userId);
        Meteor.setTimeout(function() {
            let transList = document.getElementById('modalTransList');
            transList.style.display = "block";
        }, 150);
    },
    'click .reqPayment' (event) {
        event.preventDefault();
        let userId = Meteor.userId();
        Session.set("reqType", "Request Payment");
        Session.set("requestorId", userId);
        Meteor.setTimeout(function() {
            let reqPay = document.getElementById('reqModal');
            reqPay.style.display = "block";
        }, 150);
    },
    'click .reqToBuy' (event) {
        event.preventDefault();
        let userId = Meteor.userId();
        Session.set("reqType", "Request To Buy");
        Session.set("requestorId", userId);
        Meteor.setTimeout(function() {
            let reqMod = document.getElementById('reqModal');
            reqMod.style.display = "block";
        }, 150);
    },
    'click .sendMoney' (event) {
        event.preventDefault();
        let userId = Meteor.userId();
        Session.set("reqType", "Send Money");
        Session.set("requestorId", userId);
        Meteor.setTimeout(function() {
            let reqMod = document.getElementById('reqModal');
            reqMod.style.display = "block";
        }, 150);
    }
});