import { UserConfigOptions } from "../../../imports/api/userConfigOptions.js";

Template.editUserModal.onCreated(function() {
    this.subscribe("userConfig");
});

Template.editUserModal.onRendered(function() {

});

Template.editUserModal.helpers({

});

Template.editUserModal.events({
    'click .close, click #cancelPassChange' (event) {
        $("#newPass").val("");
        $("#newPassConfirm").val("");
        let editUser = document.getElementById("editUserInfo");
        editUser.style.display = "none";
    },
    'click #savePasswordChange' (event) {
        let userId = Session.get("editUserPass");
        let pass = $("#newPass").val();
        let passConf = $("#newPassConfirm").val();
        
        if (pass == passConf) {
            Meteor.call('change.userPass', userId, pass, function(err, result) {
                if (err) {
                    console.log("    ERROR updating password: " + err);
                    showSnackbar("Error Updating Password!", "red");
                } else {
                    showSnackbar("Password Successfully Updated!", "green");
                }
            });
        } else {
            showSnackbar("Passwords Do Not Match!", "red");
            $("#newPassConfirm").val("");
            $("#newPass").val("");
        }
    }
});