FlowRouter.route('/dashboard', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNoRoute',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNotLoggedIn',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/reg', {
    name: 'reg',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "reg" });
    }
});

FlowRouter.route('/userMgmt', {
    name: 'userMgmt',
    action() {
        BlazeLayout.render('MainLayout', { main: 'userMgmt' });
    }
});

FlowRouter.route('/mygroups', {
    name: 'mygroups',
    action() {
        BlazeLayout.render('MainLayout', { main: 'acctGroup' });
    }
});

FlowRouter.route('/mykids', {
    name: 'mykids',
    action() {
        BlazeLayout.render('MainLayout', { main: 'kidAccts' });
    }
});

FlowRouter.route('/myacct', {
    name: 'myacct',
    action() {
        BlazeLayout.render('MainLayout', { main: 'myAcctCard' });
    }
});
